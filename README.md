# :package_name

## Install

Via Composer

``` bash
"shuvo/productadmin":"0.1.*"
```

## Usage

``` php
php artisan vendor publish
php artisan migrate
```